//permet le remplissage de la collection SurveyModel
//créé d'abord la collection SurveyModel avec loopback
//créé un suveyModelService dans l'appli avec super(surveyModel) qui lie le model a API et a appli

'use strict';

module.exports = function (server) {
 let SurveyService = server.models.SurveyModel; //List nom de la collection

   //modele survey avec cascading page1
  SurveyService.findOrCreate({
      where: { //si dans listName il n'y a pas companyList. Il le créé et le rempli avec options
      model: 'surveyModel1'
      }
    }, {        
        model : "surveyModel1",
        comment : "Déclaration accident avec option question cascading de survey",
        showProgressBar: "top",
        pages: [
          {//page 1
            name: "Identification entreprise",
            title: "Identification entreprise",
            elements: [
              {
                type: "text",
                name: "nomForm", //lien pour le bouton
                title: "Référence du Formulaire", // nom qui s'inscrit sur le bouton
                enableIf: "{nomForm} empty", //rendre le champ actif s'il n'est pas déjà rempli
                isRequired: true,
                requiredErrorText: "Veuillez saisir le nom du formulaire",
              },
              {
                type: "dropdown",
                title: "Nom de l'entreprise :",
                name: "companyName",
                choicesByUrl: {
                  path: 'companyList' //lien avec list-service
                },
                hasOther: false,
                otherText: 'Autre choix',
                choices: [],
                optionsCaption: "Sélectionnez"
              },
              {
                type: "dropdown",
                title: "Nom du service :",
                name: "serviceName",
                visibleIf: "{companyName} notempty",
                choicesByUrl: {
                  path: 'serviceList'
                },
                hasOther: false,
                otherText: 'Autre choix',
                choices: [],
                optionsCaption: "Sélectionnez"
              },
              {
                type: "dropdown",
                title: "Sélectionnez le responsable du service :",
                name: "chiefName",
                visibleIf: "{serviceName} notempty",
                choicesByUrl: {
                  path: 'chiefList'
                },
                hasOther: false,
                otherText: 'Autre choix',
                choices: [],
                optionsCaption: "Sélectionnez"
              },
            ]
          },
    
          {//page 2
            name: "Identification conducteur",
            title: "Identification conducteur",
            elements: [
              {
                type: "dropdown",
                title: "Sélectionnez le conducteur :",
                name: "driverName",
                choicesByUrl: {
                  path: 'driverList'
                },
                hasOther: false,
                otherText: 'Autre choix',
                choices: [],
                optionsCaption: "Sélectionnez"
              },
              {
                type: "checkbox",
                title: "Quel permis possédez-vous ?",
                name: "drivingLicences",
                choicesByUrl: {
                  path: 'drivingLicences'
                },
                choices: []
              },
            ]
          },
    
          {//page 3
            name: "Identification véhicule",
            title: "Identification véhicule",
            elements: [
              {
                type: "dropdown",
                title: "Sélectionnez le véhicule :",
                name: "vehicleName",
                choicesByUrl: {
                  path: 'vehiculeList'
                },
                hasOther: false,
                otherText: 'Autre choix',
                choices: [],
                optionsCaption: "Sélectionnez"
              },
            ]
          },
    
          {//page 4
            name: "Accident",
            title: "Circonstances conducteur",
            elements: [
              {
                type: "dropdown",
                title: "A quel titre le conducteur conduisait-il ?",
                name: "drivingCondition",
                choicesByUrl: {
                  path: 'drivingConditionList'
                },
                hasOther: false,
                otherText: 'Autre choix',
                choices: [],
                optionsCaption: "Sélectionnez"
              },
              {
                type: "text",
                title: "A quelle heure le conducteur a-t-il pris son service ?",
                name: 'serviceCheckingTime',
                inputType: "time"
              },
              {
                type: "radiogroup",
                title: "Le mode de déplacement utilisé était-il conforme aux procédures habituelles ?",
                name: "deplacementMode",
                choices: ["oui", "non"]
              },
              {
                type: "radiogroup",
                title: "Y avait-il un passager à bord ?",
                name: "passengerOnBoard",
                choices: ["oui", "non"]
              },
              {
                type: "radiogroup",
                title: "Le conducteur avait-il pris son service la veille de l'accident ?",
                name: "workDayBefore",
                choices: ["oui", "non"]
              },
              {
                type: "dropdown",
                title: "Dans quelle situation se trouvait le conducteur par rapport à l'horaire prévu ?",
                name: "timeSituation",
                choicesByUrl: {
                  path: 'timeSituationList'
                },
                hasOther: false,
                otherText: 'Autre choix',
                choices: [],
                optionsCaption: "Sélectionnez"
              },
              {
                type: "text",
                title: "Quelle est la distance parcourue entre la prise de service et le lieu de l'accident ?",
                name: 'ditanceBetweenCompanyAndAccident',
                inputType: "number"
              },
              {
                type: "radiogroup",
                title: "Dans les instants qui ont précédé l'accident, le conducteur était-il préoccupé ?",
                name: "preoccupiedBefore",
                choices: ["oui", "non"]
              },
              {
                type: "radiogroup",
                title: "Dans les instants qui ont précédé l'accident, le conducteur était-il distrait ?",
                name: "distractedBefore",
                choices: ["oui", "non"]
              },
              {
                type: "dropdown",
                title: "Dans les instants qui ont précédé l'accident, le conducteur se sentait-il plutôt ?",
                name: "driverFeelingBefore",
                choicesByUrl: {
                  path: 'driverFeelingList'
                },
                hasOther: false,
                otherText: 'Autre choix',
                choices: [],
                optionsCaption: "Sélectionnez"
              },
              {
                type: "text",
                name: "Avant le choc, le conducteur a eu le temps de :",
                inputType: "text"
              },
              {
                type: "text",
                name: "Autres remarques (le conducteur a-t-il remarqué quelque chose de particulier avant l'accident ?) :",
                inputType: "text"
              },
            ]
          },
    
    
          {//page 5
            name: "Circonstances véhicule entreprise",
            title: "Circonstances véhicule entreprise",
            elements: [
              {
                type: "dropdown",
                title: "Type de véhicule impliqué dans l'accident :",
                name: "vehiculeType",
                choicesByUrl: {
                  path: 'vehiculeTypeList'
                },
                hasOther: false,
                otherText: 'Autre choix',
                choices: [],
                optionsCaption: "Sélectionnez"
              },
              {
                type: "radiogroup",
                title: "Le véhicule est-il habituel au conducteur ?",
                name: 'driverHabits',
                choices: ["oui", "non"]
              },
    
              {
                type: "radiogroup",
                title: "Le véhicule était-il adapté à la mission effectuée ?",
                name: 'vehiculeAdaptation',
                choices: ["oui", "non"]
              },
              {
                type: "dropdown",
                title: "Au moment de l'accident, le véhicule était :",
                name: "chargeMode",
                choicesByUrl: {
                  path: 'chargeModeList'
                },
                hasOther: false,
                otherText: 'Autre choix',
                choices: [],
                optionsCaption: "Sélectionnez"
              },
    
              {
                type: "radiogroup",
                title: "Le chargement était-il :",
                name: 'chargement',
                choices: ["inférieur (ou égal) à la charge utile", "supérieur à la charge utile"]
              },
    
              {
                type: "radiogroup",
                title: "Le véhicule était-il à l'arrêt ou en stationnement ?",
                name: 'vehiculePosition',
                choices: ["oui", "non"]
              },
              {
                type: "dropdown",
                title: "Quel genre de stationnement ?",
                name: "parkingType",
                choicesByUrl: {
                  path: 'parkingTypeList'
                },
                hasOther: false,
                otherText: 'Autre choix',
                choices: [],
                optionsCaption: "Sélectionnez"
              },
            ]
          },
    
          {//page 6
            name: "Etat des lieux",
            renderedHtml: "multiple",
            title: "Etat des lieux",
            elements: [
              {
                type: "text",
                name: "Quel jour l'accident a-t-il eu lieu ?",
                inputType: "date",
                PlaceHolder: "jj/mm/aaaa hh:mm",
              },
              {
                type: "text",
                name: "A quelle heure l'accident a-t-il eu lieu ?",
                inputType: "time",
                PlaceHolder: "hh:mm",
              },
              {
                type: "checkbox",
                title: "Indiquer le type de sinistre :",
                name: "sinisterType",
                choicesByUrl: {
                  path: 'sinisterTypeList'
                },
                choices: []
              },
              {
                type: "checkbox",
                title: "Quelles étaient les conditions météo au moment de l'accident :",
                name: "weatherCondition",
                choicesByUrl: {
                  path: 'weatherConditionList'
                },
                hasOther: false,
                otherText: 'Autre choix',
                choices: [],
                optionsCaption: "Sélectionnez"
              },
              {
                type: "radiogroup",
                title: "Quelles étaient les conditions de lumière au moment de l'accident :",
                name: "lightCondition",
                choices: ["aube", "journée", "crépuscule"]
              },
              {
                type: "dropdown",
                title: "Type d'environnement où l'accident s'est produit :",
                name: "environnement",
                choicesByUrl: {
                  path: 'environnementList'
                },
                hasOther: false,
                otherText: 'Autre choix',
                choices: [],
                optionsCaption: "Sélectionnez"
              },
              {
                type: "dropdown",
                title: "Précisez la configuration des lieux de l'accident :",
                name: "accidentConfig",
                choicesByUrl: {
                  path: 'accidentConfigList'
                },
                hasOther: false,
                otherText: 'Autre choix',
                choices: [],
                optionsCaption: "Sélectionnez"
              },
              {
                type: "imagepicker",
                name: "collisionType",
                title: "Indiquer le type de collision",
                showLabel: true,
                choicesByUrl:
                  {
                    path: "collisionTypeList"
                  },
                choices: []
              },
    
              {
                type: "imagepicker",
                name: "impactPoint",
                title: "Indiquer le point d'impact sur le véhicule",
                showLabel: true,
                choicesByUrl:
                  {
                    path: "impactPointList"
                  },
                choices: []
              },
              {
                type: "dropdown",
                name: "DommagesGravity",
                title: "Indiquer la gravité des dommages du véhicule :",
                choicesByUrl: {
                  path: 'dommagesGravityList'
                },
                hasOther: false,
                otherText: 'Autre choix',
                choices: [],
                optionsCaption: "Sélectionnez"
              },
              {
                type: "radiogroup",
                title: "Quelle est la responsabilité finale du véhicule de la flotte ?",
                name: 'responsability',
                choices: ["100% tort", "50% tort", "0 tort"]
              },
              {
                type: "radiogroup",
                title: "Avez-vous réalisé un constat à l'amiable ?",
                name: 'insuranceConstat',
                choices: ["oui", "non"]
              },
              {
                type: "radiogroup",
                title: "Cet accident a-t-il été déclaré auprès de l'assurance ?",
                name: 'insurance',
                choices: ["oui", "non"]
              },
            ],
          },
    
          {//page 7
            name: "Tiers",
            title: "Tiers",
            elements: [
              {
                type: "dropdown",
                title: "Indiquer le type de tiers impliqué :",
                name: "thirdType",
                choicesByUrl: {
                  path: 'thirdTypeList'
                },
                hasOther: false,
                otherText: 'Autre choix',
                choices: [],
                optionsCaption: "Sélectionnez"
              },
              {
                type: "checkbox",
                title: "Indiquer le type de sinistre subi par le tiers :",
                name: "thirdSinister",
                choicesByUrl: {
                  path: 'thirdSinisterList'
                },
                choices: [],
              },
              {
                type: "dropdown",
                title: "Préciser les circonstances du véhicule tiers au moment de l'accident :",
                name: "thirdCircumstance",
                choicesByUrl: {
                  path: 'thirdCircumstanceList'
                },
                hasOther: false,
                otherText: 'Autre choix',
                choices: [],
                optionsCaption: "Sélectionnez"
              },
            ]
          },
    
          {//page 8
            name: "Informations complémentaires",
            title: "Informations complémentaires",
            elements: [
              {
                type: "text",
                name: "Le conducteur estime sa responsabilité à (en %) :",
                inputType: "text"
              },
              {
                type: "text",
                name: "Précisions éventuelles sur les circonstances de l'accident :",
                inputType: "text"
              },
              {
                type: "radiogroup",
                name: "Pensez-vous que, dans de mêmes conditions, il soit possible d'éviter un tel accident ?",
                choices: ["oui", "non"]
              },
              {
                type: "radiogroup",
                name: "Pensez-vous que le sinistre était évitable ?",
                choices: ["oui", "non"]
              },
              {
                type: "text",
                name: "Que proposez-vous pour qu'un tel accident ne se reproduise pas ?",
                inputType: "text"
              },
            ]
          },
    
          {//page 9
            name: "Finalisatiion du rapport",
            title: "Finalisation du rapport",
            elements: []
    
          },
        ],
        pagePrevText: 'Précédent',
        pageNextText: 'Suivant',
        completeText: 'Envoyer'
      }

  ).then(list => console.log('list de checkSurveyModel1 ', JSON.stringify(list, null, 3))
  ).catch(err => console.error(err))

  //modele survey sans cascading page1
  SurveyService.findOrCreate({
    where: { //si dans listName il n'y a pas companyList. Il le créé et le rempli avec options
    model: 'surveyModel2'
    }
  }, {        
      model : "surveyModel2",
      comment : "Déclaration accident sans option question cascading de survey",
      showProgressBar: "top",
      pages: [
        {//page 1
          name: "Identification entreprise",
          title: "Identification entreprise",
          elements: [
            {
              type: "text",
              name: "nomForm", //lien pour le bouton
              title: "Référence du Formulaire", // nom qui s'inscrit sur le bouton
              enableIf: "{nomForm} empty", //rendre le champ actif s'il n'est pas déjà rempli
              isRequired: true,
              requiredErrorText: "Veuillez saisir le nom du formulaire",
            },
            {
              type: "dropdown",
              title: "Nom de l'entreprise :",
              name: "companyName",
              choicesByUrl: {
                path: 'companyList' //lien avec list-service
              },
              hasOther: false,
              otherText: 'Autre choix',
              choices: [],
              optionsCaption: "Sélectionnez"
            },
            {
              type: "dropdown",
              title: "Nom du service :",
              name: "serviceName",
              //visibleIf: "{companyName} notempty",
              choicesByUrl: {
                path: 'serviceList'
              },
              hasOther: false,
              otherText: 'Autre choix',
              choices: [],
              optionsCaption: "Sélectionnez"
            },
            {
              type: "dropdown",
              title: "Sélectionnez le responsable du service :",
              name: "chiefName",
              //visibleIf: "{serviceName} notempty",
              choicesByUrl: {
                path: 'chiefList'
              },
              hasOther: false,
              otherText: 'Autre choix',
              choices: [],
              optionsCaption: "Sélectionnez"
            },
          ]
        },
  
        {//page 2
          name: "Identification conducteur",
          title: "Identification conducteur",
          elements: [
            {
              type: "dropdown",
              title: "Sélectionnez le conducteur :",
              name: "driverName",
              choicesByUrl: {
                path: 'driverList'
              },
              hasOther: false,
              otherText: 'Autre choix',
              choices: [],
              optionsCaption: "Sélectionnez"
            },
            {
              type: "checkbox",
              title: "Quel permis possédez-vous ?",
              name: "drivingLicences",
              choicesByUrl: {
                path: 'drivingLicences'
              },
              choices: []
            },
          ]
        },
  
        {//page 3
          name: "Identification véhicule",
          title: "Identification véhicule",
          elements: [
            {
              type: "dropdown",
              title: "Sélectionnez le véhicule :",
              name: "vehicleName",
              choicesByUrl: {
                path: 'vehiculeList'
              },
              hasOther: false,
              otherText: 'Autre choix',
              choices: [],
              optionsCaption: "Sélectionnez"
            },
          ]
        },
  
        {//page 4
          name: "Accident",
          title: "Circonstances conducteur",
          elements: [
            {
              type: "dropdown",
              title: "A quel titre le conducteur conduisait-il ?",
              name: "drivingCondition",
              choicesByUrl: {
                path: 'drivingConditionList'
              },
              hasOther: false,
              otherText: 'Autre choix',
              choices: [],
              optionsCaption: "Sélectionnez"
            },
            {
              type: "text",
              title: "A quelle heure le conducteur a-t-il pris son service ?",
              name: 'serviceCheckingTime',
              inputType: "time"
            },
            {
              type: "radiogroup",
              title: "Le mode de déplacement utilisé était-il conforme aux procédures habituelles ?",
              name: "deplacementMode",
              choices: ["oui", "non"]
            },
            {
              type: "radiogroup",
              title: "Y avait-il un passager à bord ?",
              name: "passengerOnBoard",
              choices: ["oui", "non"]
            },
            {
              type: "radiogroup",
              title: "Le conducteur avait-il pris son service la veille de l'accident ?",
              name: "workDayBefore",
              choices: ["oui", "non"]
            },
            {
              type: "dropdown",
              title: "Dans quelle situation se trouvait le conducteur par rapport à l'horaire prévu ?",
              name: "timeSituation",
              choicesByUrl: {
                path: 'timeSituationList'
              },
              hasOther: false,
              otherText: 'Autre choix',
              choices: [],
              optionsCaption: "Sélectionnez"
            },
            {
              type: "text",
              title: "Quelle est la distance parcourue entre la prise de service et le lieu de l'accident ?",
              name: 'ditanceBetweenCompanyAndAccident',
              inputType: "number"
            },
            {
              type: "radiogroup",
              title: "Dans les instants qui ont précédé l'accident, le conducteur était-il préoccupé ?",
              name: "preoccupiedBefore",
              choices: ["oui", "non"]
            },
            {
              type: "radiogroup",
              title: "Dans les instants qui ont précédé l'accident, le conducteur était-il distrait ?",
              name: "distractedBefore",
              choices: ["oui", "non"]
            },
            {
              type: "dropdown",
              title: "Dans les instants qui ont précédé l'accident, le conducteur se sentait-il plutôt ?",
              name: "driverFeelingBefore",
              choicesByUrl: {
                path: 'driverFeelingList'
              },
              hasOther: false,
              otherText: 'Autre choix',
              choices: [],
              optionsCaption: "Sélectionnez"
            },
            {
              type: "text",
              name: "Avant le choc, le conducteur a eu le temps de :",
              inputType: "text"
            },
            {
              type: "text",
              name: "Autres remarques (le conducteur a-t-il remarqué quelque chose de particulier avant l'accident ?) :",
              inputType: "text"
            },
          ]
        },
  
  
        {//page 5
          name: "Circonstances véhicule entreprise",
          title: "Circonstances véhicule entreprise",
          elements: [
            {
              type: "dropdown",
              title: "Type de véhicule impliqué dans l'accident :",
              name: "vehiculeType",
              choicesByUrl: {
                path: 'vehiculeTypeList'
              },
              hasOther: false,
              otherText: 'Autre choix',
              choices: [],
              optionsCaption: "Sélectionnez"
            },
            {
              type: "radiogroup",
              title: "Le véhicule est-il habituel au conducteur ?",
              name: 'driverHabits',
              choices: ["oui", "non"]
            },
  
            {
              type: "radiogroup",
              title: "Le véhicule était-il adapté à la mission effectuée ?",
              name: 'vehiculeAdaptation',
              choices: ["oui", "non"]
            },
            {
              type: "dropdown",
              title: "Au moment de l'accident, le véhicule était :",
              name: "chargeMode",
              choicesByUrl: {
                path: 'chargeModeList'
              },
              hasOther: false,
              otherText: 'Autre choix',
              choices: [],
              optionsCaption: "Sélectionnez"
            },
  
            {
              type: "radiogroup",
              title: "Le chargement était-il :",
              name: 'chargement',
              choices: ["inférieur (ou égal) à la charge utile", "supérieur à la charge utile"]
            },
  
            {
              type: "radiogroup",
              title: "Le véhicule était-il à l'arrêt ou en stationnement ?",
              name: 'vehiculePosition',
              choices: ["oui", "non"]
            },
            {
              type: "dropdown",
              title: "Quel genre de stationnement ?",
              name: "parkingType",
              choicesByUrl: {
                path: 'parkingTypeList'
              },
              hasOther: false,
              otherText: 'Autre choix',
              choices: [],
              optionsCaption: "Sélectionnez"
            },
          ]
        },
  
        {//page 6
          name: "Etat des lieux",
          renderedHtml: "multiple",
          title: "Etat des lieux",
          elements: [
            {
              type: "text",
              name: "Quel jour l'accident a-t-il eu lieu ?",
              inputType: "date",
              PlaceHolder: "jj/mm/aaaa hh:mm",
            },
            {
              type: "text",
              name: "A quelle heure l'accident a-t-il eu lieu ?",
              inputType: "time",
              PlaceHolder: "hh:mm",
            },
            {
              type: "checkbox",
              title: "Indiquer le type de sinistre :",
              name: "sinisterType",
              choicesByUrl: {
                path: 'sinisterTypeList'
              },
              choices: []
            },
            {
              type: "checkbox",
              title: "Quelles étaient les conditions météo au moment de l'accident :",
              name: "weatherCondition",
              choicesByUrl: {
                path: 'weatherConditionList'
              },
              hasOther: false,
              otherText: 'Autre choix',
              choices: [],
              optionsCaption: "Sélectionnez"
            },
            {
              type: "radiogroup",
              title: "Quelles étaient les conditions de lumière au moment de l'accident :",
              name: "lightCondition",
              choices: ["aube", "journée", "crépuscule"]
            },
            {
              type: "dropdown",
              title: "Type d'environnement où l'accident s'est produit :",
              name: "environnement",
              choicesByUrl: {
                path: 'environnementList'
              },
              hasOther: false,
              otherText: 'Autre choix',
              choices: [],
              optionsCaption: "Sélectionnez"
            },
            {
              type: "dropdown",
              title: "Précisez la configuration des lieux de l'accident :",
              name: "accidentConfig",
              choicesByUrl: {
                path: 'accidentConfigList'
              },
              hasOther: false,
              otherText: 'Autre choix',
              choices: [],
              optionsCaption: "Sélectionnez"
            },
            {
              type: "imagepicker",
              name: "collisionType",
              title: "Indiquer le type de collision",
              showLabel: true,
              choicesByUrl:
                {
                  path: "collisionTypeList"
                },
              choices: []
            },
  
            {
              type: "imagepicker",
              name: "impactPoint",
              title: "Indiquer le point d'impact sur le véhicule",
              showLabel: true,
              choicesByUrl:
                {
                  path: "impactPointList"
                },
              choices: []
            },
            {
              type: "dropdown",
              name: "DommagesGravity",
              title: "Indiquer la gravité des dommages du véhicule :",
              choicesByUrl: {
                path: 'dommagesGravityList'
              },
              hasOther: false,
              otherText: 'Autre choix',
              choices: [],
              optionsCaption: "Sélectionnez"
            },
            {
              type: "radiogroup",
              title: "Quelle est la responsabilité finale du véhicule de la flotte ?",
              name: 'responsability',
              choices: ["100% tort", "50% tort", "0 tort"]
            },
            {
              type: "radiogroup",
              title: "Avez-vous réalisé un constat à l'amiable ?",
              name: 'insuranceConstat',
              choices: ["oui", "non"]
            },
            {
              type: "radiogroup",
              title: "Cet accident a-t-il été déclaré auprès de l'assurance ?",
              name: 'insurance',
              choices: ["oui", "non"]
            },
          ],
        },
  
        {//page 7
          name: "Tiers",
          title: "Tiers",
          elements: [
            {
              type: "dropdown",
              title: "Indiquer le type de tiers impliqué :",
              name: "thirdType",
              choicesByUrl: {
                path: 'thirdTypeList'
              },
              hasOther: false,
              otherText: 'Autre choix',
              choices: [],
              optionsCaption: "Sélectionnez"
            },
            {
              type: "checkbox",
              title: "Indiquer le type de sinistre subi par le tiers :",
              name: "thirdSinister",
              choicesByUrl: {
                path: 'thirdSinisterList'
              },
              choices: [],
            },
            {
              type: "dropdown",
              title: "Préciser les circonstances du véhicule tiers au moment de l'accident :",
              name: "thirdCircumstance",
              choicesByUrl: {
                path: 'thirdCircumstanceList'
              },
              hasOther: false,
              otherText: 'Autre choix',
              choices: [],
              optionsCaption: "Sélectionnez"
            },
          ]
        },
  
        {//page 8
          name: "Informations complémentaires",
          title: "Informations complémentaires",
          elements: [
            {
              type: "text",
              name: "Le conducteur estime sa responsabilité à (en %) :",
              inputType: "text"
            },
            {
              type: "text",
              name: "Précisions éventuelles sur les circonstances de l'accident :",
              inputType: "text"
            },
            {
              type: "radiogroup",
              name: "Pensez-vous que, dans de mêmes conditions, il soit possible d'éviter un tel accident ?",
              choices: ["oui", "non"]
            },
            {
              type: "radiogroup",
              name: "Pensez-vous que le sinistre était évitable ?",
              choices: ["oui", "non"]
            },
            {
              type: "text",
              name: "Que proposez-vous pour qu'un tel accident ne se reproduise pas ?",
              inputType: "text"
            },
          ]
        },
  
        {//page 9
          name: "Finalisatiion du rapport",
          title: "Finalisation du rapport",
          elements: []
  
        },
      ],
      pagePrevText: 'Précédent',
      pageNextText: 'Suivant',
      completeText: 'Envoyer'
    }

).then(list => console.log('list de checkSurveyModel ', JSON.stringify(list, null, 3))
).catch(err => console.error(err))

};
